import React from 'react';
import { Toaster } from 'react-hot-toast';
import AppContent from './components/AppContent';
import AppFilter from './components/AppFilter';
import AppFooter from './components/AppFooter';
import AppHeader from './components/AppHeader';
import PageTitle from './components/PageTitle';
import styles from './styles/modules/app.module.scss';

function App() {
  return (
    <>
      <div className="container">
        <PageTitle>Todo Search</PageTitle>
        <div className={styles.app__wrapper}>
          <AppHeader />
        </div>

        <PageTitle>Todo List</PageTitle>
        <div className={styles.app__wrapper}>
          <AppFilter />
        </div>

        <div className={styles.app__wrapper}>
          <AppContent />
        </div>

        <div className={styles.app__wrapper}>
          <AppFooter />
        </div>
      </div>
      <Toaster
        position="bottom-right"
        toastOptions={{
          style: {
            fontSize: '1.4rem',
          },
        }}
      />
    </>
  );
}

export default App;
