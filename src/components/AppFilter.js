import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ButtonFilter, WrapFilterButton } from './Button';
import styles from '../styles/modules/app.module.scss';
import { updateFilterStatus } from '../slices/todoSlice';

const AppFilter = () => {
  const initialFilterStatus = useSelector((state) => state.todo.filterStatus);
  const [filterStatus, setFilterStatus] = useState(initialFilterStatus);
  const dispatch = useDispatch();

  const updateFilter = (e) => {
    setFilterStatus(e.target.value);
    dispatch(updateFilterStatus(e.target.value));
  };
  return (
    <div>
      <WrapFilterButton
        className={styles.appHeader}
        id="status"
        onClick={(e) => updateFilter(e)}
        value={filterStatus}
      >
        <ButtonFilter value="all">All</ButtonFilter>
        <ButtonFilter value="complete" variant="primary">
          Done
        </ButtonFilter>
        <ButtonFilter value="incomplete" variant="primary">
          Todo
        </ButtonFilter>
      </WrapFilterButton>
    </div>
  );
};

export default AppFilter;
