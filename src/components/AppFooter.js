import React from 'react';

import { DeleteButton } from './Button';
import styles from '../styles/modules/app.module.scss';

function AppFooter() {
  return (
    <div className={styles.appHeader}>
      <DeleteButton> Delete done tasks</DeleteButton>
      <DeleteButton> Delete all task</DeleteButton>
    </div>
  );
}

export default AppFooter;
