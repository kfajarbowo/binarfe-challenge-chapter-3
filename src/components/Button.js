import React from 'react';
import styles from '../styles/modules/button.module.scss';
import { getClasses } from '../utils/getClasses';

const buttonTypes = {
  primary: 'primary',
  secondary: 'secondary',
};

function Button({ type, variant = 'primary', children, ...rest }) {
  return (
    <button
      type={type === 'submit' ? 'submit' : 'button'}
      className={getClasses([
        styles.button,
        styles[`button--${buttonTypes[variant]}`],
      ])}
      {...rest}
    >
      {children}
    </button>
  );
}

function ButtonFilter({ type, variant = 'primary', children, ...rest }) {
  return (
    <button
      type={type === 'submit' ? 'submit' : 'button'}
      className={getClasses([
        styles.button,
        styles.button__filters,
        styles[`button--${buttonTypes[variant]}`],
      ])}
      {...rest}
    >
      {children}
    </button>
  );
}

function SelectButton({ children, variant = 'primary', id, ...rest }) {
  return (
    <select
      id={id}
      className={getClasses([
        styles.button,
        styles.button__select,
        styles[`button--${buttonTypes[variant]}`],
      ])}
      {...rest}
    >
      {children}
    </select>
  );
}

function SearchButton({ children, id, ...rest }) {
  return (
    <input
      placeholder="Search here cuyyy..."
      type="text"
      className={getClasses([styles.button, styles.button__search])}
      {...rest}
      {...children}
    />
  );
}

function WrapFilterButton({ children, id, ...rest }) {
  return (
    <div id={id} className={getClasses([styles.button])} {...rest}>
      {children}
    </div>
  );
}

function DeleteButton({ type, children, ...rest }) {
  return (
    <button
      type={type === 'submit' ? 'submit' : 'button'}
      className={getClasses([styles.button, styles.button__delete])}
      {...rest}
    >
      {children}
    </button>
  );
}

export { ButtonFilter };
export { DeleteButton };
export { SearchButton };
export { WrapFilterButton };
export { SelectButton };
export default Button;
